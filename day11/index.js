import getInputs from '../getInputs.js';

const solver = (text) => {
    let array = text.trim().split('\n').map((el) => el.split('').map(Number));    

    // PART1
    let result = 0; 
    console.log(`(PART1)`,`Result is ${result}`);

    // PART2  
    let result2 = 0;

    console.log(`(PART2)`,`Result is ${result2}`);
};

const req = getInputs('11');
req.then(solver);