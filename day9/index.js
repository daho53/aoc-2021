import getInputs from '../getInputs.js';
let rowLength = 0;
let strLength = 0;
let str = '';
let done = []

const solver = (text) => {
    const array = text.trim().split('\n');
    rowLength = array.length;
    str = array.join('');
    strLength = str.length;
    let basins = [];    
    let result = 0;    

    for (let i = 0; i < str.length; i++) {
        const el = +str[i];
        const top = (i >= rowLength) ? +str[i - rowLength] : 10;
        const bot = (i + rowLength < strLength) ? +str[i + rowLength] : 10;
        const lef = (i % rowLength !== 0) ? +str[i - 1] : 10;
        const rig = ((i + 1) % rowLength !== 0) ? +str[i + 1] : 10;

        if (el < top && el < bot && el < lef && el < rig) {        
            // PART 1    
            result += el + 1;

            // PART 2   
            let basin = basinCount(i, 0);
            basins.push(basin);
        }
    }

    // PART1
    console.log(`(PART1)`,`Result is ${result}`);

    // PART2   
    basins = basins.sort((a, b) => b - a);
    let result2 = basins[0] * basins[1] * basins[2];

    console.log(`(PART2)`,`Result is ${result2}`);
};

const basinCount = (i, count) => {
    if (done.includes(i)) return count;

    count++;

    const topi = i - rowLength;
    const boti = i + rowLength;
    const lefi = i - 1;
    const rigi = i + 1;

    const top = (i >= rowLength) ? +str[topi] : 10;
    const bot = (boti < strLength) ? +str[boti] : 10;
    const lef = (i % rowLength !== 0) ? +str[lefi] : 10;
    const rig = (rigi % rowLength !== 0) ? +str[rigi] : 10;

    done.push(i);

    if (top < 9) count = basinCount(topi, count);
    if (rig < 9) count = basinCount(rigi, count);
    if (bot < 9) count = basinCount(boti, count);
    if (lef < 9) count = basinCount(lefi, count);

    return count;
};

const req = getInputs('9');
req.then(solver);