import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split(',').map(Number);
    const array2 = [...array];
    console.log(array);
    
    // PART1
    const MAXDAY = 80;
    for (let i = 0; i < MAXDAY; i++) {
        let push = [];
        for (let j = 0; j < array.length; j++) {
            array[j]--;
            if (array[j] >= 0) continue;

            array[j] = 6;
            push.push(8);                      
        }
        array.push(...push);
    }

    let result = array.length;

    console.log(`(PART1)`,`Result is ${result}`);

    // PART2
    const DOOMDAY = 256;
    let fishBuckets = [0,0,0,0,0,0,0,0,0];
    array2.forEach(val => fishBuckets[val]++);

    let newFish = 0;
    for (let i = 0; i < DOOMDAY; i++) {
        fishBuckets[7] += fishBuckets[0];
        newFish = fishBuckets.shift();
        fishBuckets.push(newFish);
    }

    // console.log(fishBuckets);

    let result2 = fishBuckets.reduce((a, b) => a + b);

    console.log(`(PART2)`,`Result is ${result2}`);
};

const req = getInputs('6');
req.then(solver);