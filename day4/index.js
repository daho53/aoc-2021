import getInputs from '../getInputs.js';
let numbers = [];
let boards = [];
let boards2 = [];

const solver = (text) => {
    let array = text.trim().split('\n');
    numbers = array[0].split(',');
    for (let i = 1; i < array.length; i++) {
        if (array[i] === '') {
            boards.push([]);
        } else {
            array[i] = array[i].trim().replace(/  /g, ' ');
            boards[boards.length - 1].push(array[i].split(' '));
        }
    }
    
    // PART1    
    let result = null;
    loop1: for (let i = 4; i < numbers.length; i++) {
        loop2: for (let j = 0; j < boards.length; j++) {
            result = checker(boards[j], i);
            if (!result) continue;
            break loop1;            
        }
    }

    console.log(`(PART1)`,`Result is ${result}`);

    // PART2
    let done = [];
    let result2 = null;
    loop1: for (let i = 4; i < numbers.length; i++) {
        loop2: for (let j = 0; j < boards.length; j++) {
            if (done.includes(j)) continue;
            
            result2 = checker(boards[j], i);
            if (!result2) continue;
            
            done.push(j);
            if (done.length < boards.length) continue;
            
            break loop1;
        }
    }

    console.log(`(PART2)`,`Result is ${result2}`);    
};

const checker = (board, lastNumberIndex) => {
    let result = null;
    let win = false;
    let sum = 0;
    const number = numbers[lastNumberIndex];
    const currNumbers = [...numbers];
    currNumbers.length = lastNumberIndex + 1;

    for (const [i, row] of board.entries()) {
        let colwin = true;
        let rowwin = true;
        for (const [j, val] of row.entries()) {
            if (currNumbers.indexOf(val) == -1) {
                sum += parseInt(val);
                rowwin = false;
            }
            let colVal = board[j][i]; 
            if (currNumbers.indexOf(colVal) == -1) {
                colwin = false;
            }
            if (j === 4 && (rowwin || colwin)) win = i;
        }
    }

    if (win !== false) {
        result = sum * number;
        // console.log('Board', win);
        // console.log('Number', number);
        // console.log('Sum', sum);
    }
    return result;
};

const req = getInputs('4');
req.then(solver);