const COOKIE = '53616c7465645f5fcd8c3df1ed7adf5ea37f6d2c6e73874e4f84b7b581d6853ee479665834ffb68fb21fb4c025978a6f';

import fetch from 'node-fetch';

export default function getInputs(day = '1', cookie = COOKIE) {
    return new Promise((resolve, reject) => {
        fetch(`https://adventofcode.com/2021/day/${day}/input`, 
            {
                credentials: 'include', 
                headers: {
                    Cookie: `session=${cookie}`
                }
            })
        .then(function(response) {
            response.text().then(function(text) {
                resolve(text);
            });
        });
    });
}