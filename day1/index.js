import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split('\n');
    // console.log(array);
    
    // PART1
    let newArray = array.filter((measurment, index) => {
        const prevMeasurment = array[index - 1];
        return prevMeasurment && parseInt(measurment) > parseInt(prevMeasurment)
    });

    console.log(`(PART1)`,`${newArray.length} of ${array.length} measurements are larger than the previous measurement`);

    // PART2
    let triplesArray = [];
    let largerTriples = 0;
    for (const [index, measurment] of array.entries()) {
        if (index < 2) continue;
        const triple = parseInt(array[index]) + parseInt(array[index - 1]) + parseInt(array[index - 2]);
        triplesArray.push(triple);
        if (triplesArray.length < 2) continue;
        if (triple > triplesArray[triplesArray.length - 2]) largerTriples++;
    }

    console.log(`(PART2)`,`${largerTriples} of ${triplesArray.length} triple measurements are larger than the previous triple measurement`);
};

const req = getInputs('1');
req.then(solver);