import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split(',').map(Number).sort((a,b) => a - b);
    // console.log(array);
    
    // PART1
    const MAX = Math.max(...array);
    const MIN = Math.min(...array);
    const SUM = array.reduce((a, b) => a + b, 0);
    const AVG = SUM / array.length;
    const MED = median(array);

    let minfuel = null;
    let minpos = null;
    // for (let i = MIN; i <= MAX; i++) {
    for (let i = MED; i <= AVG; i++) {
        let fuel = 0;
        for (let j = 0; j < array.length; j++) {
            fuel += Math.abs(array[j] - i);
        }
        if (!minfuel || fuel < minfuel) {
            minfuel = fuel;
            minpos = i;
        }
    }
    let result = minfuel;

    console.log(`(PART1)`,`Min fuel is ${result}, Best position ${minpos}`);

    // PART2
    let minfuel2 = null;
    let minpos2 = null;
    // for (let i = MIN; i <= MAX; i++) {
    for (let i = MED; i <= AVG; i++) {
        let fuel = 0;
        for (let j = 0; j < array.length; j++) {
            let diff = Math.abs(array[j] - i);
            fuel += diff * (diff + 1) / 2;
        }
        if (!minfuel2 || fuel < minfuel2) {
            minfuel2 = fuel;
            minpos2 = i;
        }
    }
    let result2 = minfuel2;

    console.log(`(PART2)`,`Min fuel is ${result2}, Best position ${minpos2}, AVG ${AVG}, MED ${MED}`);
};

const median = (arr) => {
    arr.sort((a, b) => a - b);
    let l = arr.length / 2;
    return l % 1 == 0 ? (arr[l - 1] + arr[l]) / 2 : arr[Math.floor(l)];
}

const req = getInputs('7');
req.then(solver);