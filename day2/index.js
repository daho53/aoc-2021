import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split('\n');   
    // console.log(array);
    
    // PART1
    let forward = 0;
    let depth = 0;    
    for (const action of array) {
        const res = action.split(' ');
        const command = res[0];
        const distance = parseInt(res[1]);

        if (command === 'forward')  forward += distance;
        if (command === 'down')     depth   += distance;
        if (command === 'up')       depth   -= distance;
    }
    let multiply = forward * depth;

    console.log(`(PART1)`,`Forward: ${forward}, Depth ${depth}, Multiply: ${multiply}`);

    // PART2
    let forward2 = 0;
    let depth2 = 0;    
    let aim2 = 0;    
    for (const action of array) {
        const res = action.split(' ');
        const command = res[0];
        const distance = parseInt(res[1]);

        if (command === 'forward') {
            forward2 += distance;
            depth2   += aim2 * distance;
        }
        if (command === 'down') aim2 += distance;
        if (command === 'up')   aim2 -= distance;
    }
    let multiply2 = forward2 * depth2;

    console.log(`(PART2)`,`Forward: ${forward2}, Depth ${depth2}, Multiply: ${multiply2}`);
};

const req = getInputs('2');
req.then(solver);