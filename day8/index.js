import getInputs from '../getInputs.js';

const solver = (text) => {
    // PART1
    const array10 = [];
    const array1 = text.trim().split('\n').map((val) => {
        let res = val.split(' | ');
        let res2 = res[1].split(' ');
        array10.push(...res2);
        return res[0];
    });
    
    const rightValues = array10.filter((el) => [2,4,3,7].indexOf(el.length) != -1);

    console.log(`(PART1)`,`Result is ${rightValues.length}`);

    // PART2    
    const array2 = text.trim().split('\n').map((val) => {
        let res = val.split(' | ');
        let res2 = res[1].split(' ');
        let res3 = res[0].split(' ');
        res3.push(...res2);
        return res3;
    });
    // console.log(array20);

    let result2 = 0;
    for (let i = 0; i < array2.length; i++) {
        const el2 = array2[i];
        
        let one = null;
        let four = null;
        array2[i] = array2[i].map((val) => {
            if (val.length === 3) return 7;
            if (val.length === 7) return 8;
            if (val.length === 2) {
                one = val;
                return 1;
            }
            if (val.length === 4) {
                four = val;
                return 4;
            }
            return val;
        });

        let C = one[0];
        let F = one[1];
        four = four.split(C).join('').split(F).join('');
        let B = four[0];
        let D = four[1];
        array2[i] = array2[i].map((val) => {
            if (val.length === 5) {
                if (val.includes(C) && val.includes(F)) return 3;
                if (val.includes(B) && val.includes(D)) return 5;
                if (val.includes(B) || val.includes(D)) return 2;
                return val;
            }
            if (val.length === 6) {
                if ((val.includes(C) && !val.includes(F)) || (!val.includes(C) && val.includes(F))) return 6;
                if (val.includes(B) && val.includes(D)) return 9;
                if (val.includes(C) && val.includes(F) && (val.includes(B) || val.includes(D))) return 0;
                return val;
            }
            return val;
        });

        result2 += array2[i][13] + 10 * array2[i][12] + 100 * array2[i][11] + 1000 * array2[i][10];
    }

    console.log(`(PART2)`,`Result is ${result2}`);
};

const req = getInputs('8');
req.then(solver);