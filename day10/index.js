import getInputs from '../getInputs.js';

const PAR = {
    "ok": [0, 0],
    ")":  [3, 1],
    "]":  [57, 2],
    "}":  [1197, 3],
    ">":  [25137, 4]
};
const CLOSE =  {
    "(": ")", 
    "[": "]", 
    "{": "}", 
    "<": ">"
};
const OPEN =  ["(", "[", "{", "<"];

const solver = (text) => {
    const array = text.trim().split('\n');

    let result = 0;    
    let result2 = [];

    for (let i = 0; i < array.length; i++) {
        const el = array[i];      
        const value = rowValue(el);
        const {part1, part2} = {...value};
        
        result += part1;   
        if (part2) result2.push(part2);   
    }

    // PART1
    console.log(`(PART1)`,`Result is ${result}`);

    // PART2  
    result2 = result2.sort((a,b) => b - a);
    const index = (result2.length - 1) / 2; 

    console.log(`(PART2)`,`Result is ${result2[index]}`);
};

const rowValue = (row) => {
    let value = 'ok';
    let opened= [];

    for (let i = 0; i < row.length; i++) {
        const el = row[i];

        if (OPEN.indexOf(el) != -1) {
            opened.push(CLOSE[el]);
            continue;            
        } 
        if (opened.length < 1 || opened[opened.length - 1] !== el) {
            value = el;
            break;
        }
        opened.pop();
    }

    let completionResult = 0;
    if (value === 'ok') {
        for (let i = opened.length - 1; i >= 0; i--) {
            const el = opened[i];
            completionResult *= 5;
            completionResult += PAR[el][1];
        }
    }

    return {
        part1: PAR[value][0], 
        part2: completionResult
    };
};

const req = getInputs('10');
req.then(solver);