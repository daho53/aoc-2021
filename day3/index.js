import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split('\n');   
    // console.log(array);
    
    // PART1
    const halflength = array.length / 2;
    const binlength = array[0].length;
    let onesCount = [0,0,0,0,0,0,0,0,0,0,0,0];
    let gamma = '';
    let epsilon = '';    
    for (const value of array) {
        for (let i = 0; i < binlength; i++) {
            if (value[i] == '1') onesCount[i]++;
        }
    }
    for (const bit of onesCount) {                
        gamma   += (bit >= halflength) ? '1' : '0';
        epsilon += (bit >= halflength) ? '0' : '1';
    }
    const multiply = parseInt(gamma, 2) * parseInt(epsilon, 2);

    console.log(`(PART1)`,`Gamma:  ${gamma} (${parseInt(gamma, 2)}), Epsilon ${epsilon} (${parseInt(epsilon, 2)}), Multiply: ${multiply}`);

    // PART 2
    let oxygenArray = [...array];
    let co2Array = [...array];
    let oxygen = null;
    let co2 = null;

    for (let i = 0; i < binlength + 1; i++) {
        if (oxygenArray.length > 1){
            let gamma2 = finder(oxygenArray, i, true);
            oxygenArray = filterArray(oxygenArray, i, gamma2);
        } else if (oxygen == null) {
            oxygen = oxygenArray[0];
        }

        if (co2Array.length > 1){
            let epsilon2 = finder(co2Array, i, false);
            co2Array = filterArray(co2Array, i, epsilon2);
        } else if (co2 == null) {
            co2 = co2Array[0];
        }
    }
    const multiply2 = parseInt(oxygen, 2) * parseInt(co2, 2);

    console.log(`(PART2)`,`Oxygen: ${oxygen} (${parseInt(oxygen, 2)}), CO2     ${co2} (${parseInt(co2, 2)}), Multiply: ${multiply2}`);
};

const finder = (array, i, most = true) => {
    let ones = 0;
    for (const value of array) {
        if (value[i] == '1') ones++;            
    }
    if (most){
        return (ones >= array.length/2) ? '1' : '0';
    } else {
        return (ones >= array.length/2) ? '0' : '1';
    }    
};

const filterArray = (array, i, comparison) => {
    return array.filter((measurment, index) => {
        return measurment[i] == comparison
    })
};

const req = getInputs('3');
req.then(solver);