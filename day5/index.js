import getInputs from '../getInputs.js';

const solver = (text) => {
    const array = text.trim().split('\n');
    // console.log(array);
    
    // PART1   
    let result = 0;
    let pairs = [];
    let numbers = [];
    for (const val of array) {
        const res1 = val.split(',');
        const res2 = res1[1].split(' -> ');
        pairs.push([ [ parseInt(res1[0]), parseInt(res2[0]) ], [ parseInt(res2[1]), parseInt(res1[2]) ] ]);
        numbers.push(parseInt(res1[0]), parseInt(res2[0]), parseInt(res2[1]), parseInt(res1[2]));
    }
    const MAX = Math.max(...numbers) + 1;
    let MATRIX = new Array(MAX);    
    for (let i = 0; i < MATRIX.length; i++) {
        MATRIX[i] = new Array(MAX).fill(0);
    }

    for (let i = 0; i < pairs.length; i++) {
        let x = null;
        let y = null;            
        if (pairs[i][0][0] === pairs[i][1][0]) x = pairs[i][0][0];
        if (pairs[i][0][1] === pairs[i][1][1]) y = pairs[i][0][1];

        if (x !== null) {
            let y1 = Math.min(pairs[i][0][1], pairs[i][1][1]);
            let y2 = Math.max(pairs[i][0][1], pairs[i][1][1]);
            // console.log('x: ', x, 'y1: ', y1, 'y2: ', y2);

            for (let j = y1; j <= y2; j++) {
                MATRIX[j][x]++;
                if (MATRIX[j][x] === 2) result++;
            }
        } else if (y !== null) {
            let x1 = Math.min(pairs[i][0][0], pairs[i][1][0]);
            let x2 = Math.max(pairs[i][0][0], pairs[i][1][0]);
            // console.log('y: ', y, 'x1: ', x1, 'x2: ', x2);

            for (let j = x1; j <= x2; j++) {
                MATRIX[y][j]++;
                if (MATRIX[y][j] === 2) result++;
            }
        }
    }

    console.log(`(PART1)`,`Result is ${result}`);

    // PART2
    let result2 = 0;
    let MATRIX2 = new Array(MAX);    
    for (let i = 0; i < MATRIX2.length; i++) {
        MATRIX2[i] = new Array(MAX).fill(0);
    }

    for (let i = 0; i < pairs.length; i++) {
        let x = null;
        let y = null;            
        if (pairs[i][0][0] === pairs[i][1][0]) x = pairs[i][0][0];
        if (pairs[i][0][1] === pairs[i][1][1]) y = pairs[i][0][1];
        
        if (x !== null) {
            let y1 = Math.min(pairs[i][0][1], pairs[i][1][1]);
            let y2 = Math.max(pairs[i][0][1], pairs[i][1][1]);
            // console.log('x: ', x, 'y1: ', y1, 'y2: ', y2);

            for (let j = y1; j <= y2; j++) {
                MATRIX2[j][x]++;
                if (MATRIX2[j][x] === 2) result2++;
            }
        } else if (y !== null) {
            let x1 = Math.min(pairs[i][0][0], pairs[i][1][0]);
            let x2 = Math.max(pairs[i][0][0], pairs[i][1][0]);
            // console.log('y: ', y, 'x1: ', x1, 'x2: ', x2);

            for (let j = x1; j <= x2; j++) {
                MATRIX2[y][j]++;
                if (MATRIX2[y][j] === 2) result2++;
            }
        } else {
            let x1 = pairs[i][0][0];
            let x2 = pairs[i][1][0];
            let y1 = pairs[i][0][1];
            let y2 = pairs[i][1][1];
            if (x1 > x2) {let z = x1; x1 = x2; x2 = z; z = y1; y1 = y2; y2 = z;}

            let count = 0;
            for (let j = x1; j <= x2; j++) {
                let w = (y1 > y2) ? y1 - count : y1 + count;
                count++;
                MATRIX2[w][j]++;
                if (MATRIX2[w][j] === 2) result2++;
            }
        }
    }

    console.log(`(PART2)`,`Result is ${result2}`);
};

const req = getInputs('5');
req.then(solver);